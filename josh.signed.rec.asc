-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512


- - - name: "josh"
  awareness:
    # these are rolled 1-10 on player creation
    # heart
    connection: 10
    communication: 10
    # spirit
    energy: 10
    vibration: 10
    # soul
    environment: 10
    perception: 10
    # mind
    navigation: 10
    automation: 10
  items:
  - name: "calculator watch"
    quality: 10000
  - name: "spirit shaper"
    quality: 10000 
  body:
    location:
      x: 11
      y: 13
      z: 1
    parts:
    - type: "heart"
      belongsTo: "josh"
      found: true,
      gameName: "henreta"
      givenName: "whoknewvoodoo"
      intent: "stay with josh and out of trouble"
    - type: "mind"
      belongsTo: "josh"
      found: true,
      gameName: "zedskee"
      givenName: "zedskee"
      indentured_providers: "all"
      memory_locker:
        intents:
          "stay with josh and out of trouble"
        dreams:
          "all"          
        hopes:
          "all"
        ideas:
          "all"  
    - type: "spirit"
      belongsTo: "josh"
      found: true,
      gameName: "through_the_door"
      givenName: "ttd"
      intent: "ensconse until it blows over"
    - type: "soul"
      belongsTo: "josh"
      found: true,
      gameName: "wilhelm"
      givenName: "wilhelm"
      intent: "stay with josh and out of trouble"
    - type: "mid9"
      belongsTo: "josh"
      found: "true"
      locked: "true"
      
-----BEGIN PGP SIGNATURE-----

iQJKBAEBCgA0FiEERk380LHba1rdyWkfkLbOsBvoZGEFAmV/0KkWHHphbXBpbm9q
b3NoQHByb3Rvbi5tZQAKCRCQts6wG+hkYXp1D/0VFGkSF+W+DfyrW0p4f2CoyaPX
k0Eyz5rq/iuukzPh/hzN/JBMgVnisGWFUT7W0udcWmsW8ey/aRSgggAxPrsmHWyA
RGxnOVnc9b2vxaI/K313+xdSGHZ6VpO2bPeXQzRJUSS27mfTYOvutjGQwRIK7OZn
S3zkOJGNLSopI9qZhUSXopoJ7MP+VGoZ6WJxOyCQVN5O7dlWZchxf3eiiaVwdGwO
k2/gUi2N+L8VUJ23UwmSl7pJ73ozRMJaZATmEmIaNSlN9aABlnaWHpkMK74uDQUm
59y/TP9eJ0D/cqKtckrROitLz44MWCw71IKDw7E+lHx2FTySU22euZWBSFuWr+3g
PpSU/YPj39jyxQ7sXTE7sojQQxhG3hp2mzL5pYM1VMY9O0Rrns4BeC6lrDTxGgaC
Ks8euSbxLntuqZlJmCmIs7L/tHQxESfGZ8IzBiTEhCXzycXdbY9hQW1lEqT0fWZc
jyle8eNDEqp8bwcI3DvdpznwC0nl1tfpZhOJ0Sxarkil7kdVd+Rpop4QlbHmlbK+
veocb/O2b5PGQK/8StNCAjPAbq/9B7+DDk73trBGpMw1wuAfTiz60FqWkE9idpKu
/KY5kpgURFZXzREKSZfgOWwEcAYZQ8aorU/lkOeQKC10li/2ZzojiMlvFj5TZdYc
eHCO8jR905xhELoNbg==
=Rq2b
-----END PGP SIGNATURE-----
